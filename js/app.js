(function(Style){
    var Map = null;

    var Config = {
	    account: 'brylejoseph',

    }


    var defaultLayer = {
	    user_name: Config.account,
	    type: 'cartodb',
	    sublayers: [
            {
                sql: "SELECT * FROM layer1stores",
                cartocss: "#layer1stores {marker-fill: #5CA2D1; marker-width: 50; marker-height: 50}"
            },
		    {
            	sql: "SELECT * FROM layer2orders",
            	cartocss: Style.by_age_style
  		    }
  	    ]
    };

   
    var Polygon = {
        polygons: {},
        add: function(key, coordinates){
            var points = [];
            for (var i = 0; i < coordinates.length; i++) {
                var coord = coordinates[i];
                points.push({
                    lat: coord[1],
                    lng: coord[0]
                });
            }

            this.polygons[key] = new google.maps.Polygon({
                paths: points,
                strokeColor: '#FFFF00',
                strokeOpacity: 0.8,
                strokeWeight: 1,
                fillColor: '#FFA300',
                fillOpacity: 0.20
            });
            this.polygons[key].setMap(Map);

            google.maps.event.addListener(this.polygons[key], 'click', function() {
              var infoWindow = new google.maps.InfoWindow();
              infoWindow.setContent("Point Count : " + point_count);

              infoWindow.setPosition(new google.maps.LatLng(points[0].lat ,points[0].lng ));
              infoWindow.open(Map);
            });
        },
        get: function(key){
            return this.polygons[key];
        },
        all: function(){
            return this.polygons;
        }
    };
    var x=new google.maps.LatLng(14.558990,121.022378);
    var POC = {
	    layers: [],
	    init : function(){
            Map = new google.maps.Map(document.getElementById('map'),{
                zoom: 16,
        	    center:new google.maps.LatLng(14.558990,121.022378)
            });
            this.initializePolygons();
            this.initLayers();
	    },
	    initializePolygons: function() {
	        var query = "SELECT * FROM heatmappolygon";
            var url = 'https://brylejoseph.cartodb.com/api/v2/sql?format=GeoJSON&q='+query;
	        
            $.getJSON(url)
                .done(function(data) {
                    console.log(data);
                    $.each(data.features, function(key, feature) {
                        Polygon.add(feature.properties.gridcode, feature.geometry.coordinates[0][0], feature.properties.point_count);
                    });
                });
        },
	    initLayers: function(){
            cartodb.createLayer(Map, defaultLayer)
		        .addTo(Map)
		        .on('done', function(layer) {
                    console.log(layer);
		            POC.createSelector(layer.getSubLayer(1));
		        })
		        .on('error', function(err) {
		            alert("some error occurred: " + err);
		        });
	    },
	    createSelector: function(layer) {
            var cartocss = "";
            var $options = $(".layer_selector").find("li");

            $options.click(function(e) {
		        var $li = $(e.target);
		        var type = $li.data('type');
		        var selected = $li.attr('data');

		        console.log(type);

		        $options.removeClass('cartocss_selected');
		        $li.addClass('cartocss_selected');

		        if (type=="cartocss"){
		            cartocss = Style[selected];
		            layer.setCartoCSS(cartocss);
		        }else{
		            layer.setSQL("SELECT * FROM  layer2orders WHERE " + selected);
		        }
            });
	    }
       
    }//end of POC

    POC.init();
})(Style);
